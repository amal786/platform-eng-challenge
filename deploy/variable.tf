variable "prefix" {
  default = "onapp"
}

variable "project" {
  default = "platform-eng-challenge"
}

variable "contact" {
  default = "amal.misra@gmail.com"
}
