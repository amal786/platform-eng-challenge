# This file is maintained automatically by "terraform init".
# Manual edits may be lost in future updates.

provider "registry.terraform.io/hashicorp/aws" {
  version     = "3.1.0"
  constraints = "~> 3.1.0"
  hashes = [
    "h1:IrzgN/FjvduEr5YRmkbbVPhBMK+6l+lMi3IsaB75u9k=",
    "zh:0d9fdd0c845bdeaa968331bcb0988c1db21c3183bb5cfbe70fdf199044c246b8",
    "zh:149fc3a945813356487a6909d0ac54945b4bf02d5ae36a858c67a27193aec245",
    "zh:23af3656965d470a1354fdcb4e093a52cdfbc0ab1ebe026ea5026104a39e96bd",
    "zh:2ea03eb20a53c3f3a284882c96464e8c240e61dcafec0b8be60a03aa2ce48f50",
    "zh:460d8e509cede2eae287b3a2240916d24747cbf8fc5b2f1eb76e0fc779a923a6",
    "zh:46525d6d3d81005af6bb3301a5fb4f0254526ec7f41fdfdc745d5ff0fd1e8a7a",
    "zh:64f27ed42cc843fa06d77f709e85257102bfe8e29307704d7d94a7a18f976a46",
    "zh:66412a5d0f12c54f7b85bc4344c83693c4fc8c8e7623764665c547b27c097c5c",
    "zh:6dde864994ff19a34227f7ff6f501e985021996eb7450d65cea30eeb180f9f73",
    "zh:6ff058e8aa41393c9a4c2bd308e68daca7d4d62d0406f06b212a45e41d263c75",
  ]
}
