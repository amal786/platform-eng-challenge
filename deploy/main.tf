terraform {
  backend "s3" {
    bucket         = "platform-eng-challenge-tfstate"
    key            = "platform-eng-challenge.tfstate"
    region         = "ap-south-1"
    encrypt        = true
    dynamodb_table = "platform-eng-challenge-tf-state-lock"
  }
}

provider "aws" {
  region  = "ap-south-1"
  version = "~> 3.1.0"
}

locals {
  prefix = "${var.prefix}-${terraform.workspace}"
  common_tags = {
    Environment = terraform.workspace
    Project     = var.project
    Owner       = var.contact
    ManagedBy   = "Terraform"
  }
}

